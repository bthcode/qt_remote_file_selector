# qt_remote_file_selector

File selector dialog that allows a user to select a file on a remote host using SSH.

See example.py for usage

Requirements:
    PyQt5 
    paramiko

Screenshot:
![alt text](screenshot.png "Screenshot")

