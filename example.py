import sys
from PyQt5.QtWidgets import *
from qtpy.QtGui import *
from qtpy.QtGui import *
from qtpy.QtCore import *
from qtpy.QtWidgets import *

from QRemoteFileSelector import QRemoteFileSelector

class RemoteFileWindow(QMainWindow):
    def __init__(self, user='', host='', directory=''):
        QMainWindow.__init__(self)
        win = QWidget()
        self.setCentralWidget(win)
        self.resize(600,300)
        self.show()
        chooser = QRemoteFileSelector(user=user,host=host, directory=directory)
        chooser.exec_()
        print ("chooser.success = {}, chooser.file = {}, chooser.path = {}".format(chooser.success, chooser.file, chooser.path))
    def closeEvent(self, event):
        self.close()


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser('')
    parser.add_argument('user')
    parser.add_argument('host')
    parser.add_argument('--directory', default=None)

    args = parser.parse_args()

    app = QApplication(sys.argv)
    R = RemoteFileWindow(args.user, args.host, args.directory)
    sys.exit(app.exec_())
