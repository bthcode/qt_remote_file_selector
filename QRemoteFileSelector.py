import sys
from PyQt5.QtWidgets import *
from qtpy.QtGui import *
from qtpy.QtGui import *
from qtpy.QtCore import *
from qtpy.QtWidgets import *

from stat import S_ISDIR, S_ISREG
import paramiko

class PasswordPrompt(QDialog):
    '''
    Prompt User for Hostname, Username, Password

    When the dialog closes the following attributes will be set:
        success : if true 'ok' was clicked, else 'cancel'
        host : str : hostname or ip
        user : str : username
        password : str : password
    '''
    def __init__(self, parent=None, host='', user='', password=''):
        '''
        Arguments
        =========
            host : str : hostname or ip
            user : str : username
            password: str : user password
        '''

        super(PasswordPrompt, self).__init__(parent)
        self.host = host
        self.user = user
        self.password = password
        self.success = False
        self.initUI()
    # end __init__

    def initUI(self):
        print ('password prompt init ui')
        layout = QFormLayout()

        host_label=QLabel('Host')
        self.host_entry = QLineEdit(self.host)
        layout.addRow(host_label, self.host_entry)

        user_label = QLabel('User')
        self.user_entry = QLineEdit(self.user)
        layout.addRow(user_label, self.user_entry)

        password_label = QLabel('Password')
        self.password_entry = QLineEdit(self.password)
        self.password_entry.setEchoMode(QLineEdit.Password)
        layout.addRow(password_label, self.password_entry)

        cancel_button = QPushButton('Cancel')
        cancel_button.clicked.connect(self.on_cancel)
        ok_button = QPushButton('Ok')
        ok_button.clicked.connect(self.on_ok)
        layout.addRow(cancel_button, ok_button)

        self.setLayout(layout)
        self.show()
    # end initUI

    def on_cancel(self):
        self.success = False
        self.close()
    # end on_cancel

    def on_ok(self):
        self.success = True
        self.close()
    # end on_ok
# end class PasswordPrompat

class QRemoteFileSelector(QDialog):
    '''
    Prompt User for File Selection via SSH

    When the dialog closes the following attributes will be set:
        success : if true 'ok' was clicked, else 'cancel'
        path : str : Directory path
        file : str : Filename
    '''

    def __init__(self, parent=None, user='', host='', directory=None, password=''):
        '''
        Attributes
        ==========
            user : str : username
            host : str : hostname
            password : str : password
        '''
        super(QRemoteFileSelector, self).__init__(parent)
        self.client = None
        self.user = user
        self.host = host
        self.password = ''
        self.prompt = None
        self.initConnection()
        self.initUI()
        self.file = ''
        self.path = ''
        self.success = True
        self.render(directory)
    # end __init__

    def render(self, directory):
        self.left_list.clear()
        self.right_list.clear()

        prev_dir = self.sftp.getcwd()

        if directory is not None:
            try:
                self.sftp.chdir(directory)
            except:
                print ("Cannot change directory")
                return

        left_ctr  = 1
        right_ctr = 0
        entries = {}

        self.cwd_label.setText("Dir: {}".format(self.sftp.getcwd()))

        item = QListWidgetItem()
        item.setText('..')
        self.left_list.insertItem(0, item)

        try:
            for entry in self.sftp.listdir_attr():
                entries[ entry.filename ] = entry
        except:
            self.sftp.chdir(prev_dir)

        for key in sorted(entries.keys()):
            entry = entries[key]
            mode = entry.st_mode
            if S_ISDIR(mode):
                item = QListWidgetItem()
                item.setText(entry.filename)
                self.left_list.insertItem(left_ctr, item)
                left_ctr+=1
            elif S_ISREG(mode):
                item = QListWidgetItem()
                item.setText(entry.filename)
                self.right_list.insertItem(right_ctr, item)
                right_ctr += 1
    # end render

    def dir_double_clicked(self, evt):
        directory = evt.text()
        self.render(directory)
    # end dir_double_clicked

    def initConnection(self):
        connected = False
        username = self.user
        password = self.password
        host = self.host
        while not connected:
            try:
                self.ssh = paramiko.SSHClient()
                self.ssh.load_system_host_keys()
                if password != '':
                    self.ssh.connect(host, username=username, password=password)
                else:
                    self.ssh.connect(host, username=username)
                self.sftp = self.ssh.open_sftp()
                connected = True

            except paramiko.ssh_exception.AuthenticationException as err:
                print (err)
                connected = False
                prompt = PasswordPrompt(user=self.user,host=self.host,password='')
                prompt.exec_()
                username = prompt.user_entry.text()
                host = prompt.host_entry.text()
                password = prompt.password_entry.text()
                prompt.close()
    # end initConnection

    def initUI(self):
        main_sizer  = QVBoxLayout()

        self.cwd_label = QLabel()
        main_sizer.addWidget(self.cwd_label)

        splitter    = QSplitter(Qt.Horizontal)
        left_frame  = QFrame()
        right_frame = QFrame()
        left_sizer  = QVBoxLayout()
        right_sizer = QVBoxLayout()
        left_label  = QLabel("Directories:")
        right_label = QLabel("Files:")
        left_sizer.addWidget(left_label)
        right_sizer.addWidget(right_label)
        left_frame.setLayout(left_sizer)
        right_frame.setLayout(right_sizer)
        splitter.addWidget(left_frame)
        splitter.addWidget(right_frame)
        splitter.setSizes([200,400])
        main_sizer.addWidget(splitter)
        self.setLayout(main_sizer)
        self.left_list = QListWidget()
        self.right_list = QListWidget()
        left_sizer.addWidget(self.left_list)
        right_sizer.addWidget(self.right_list)


        button_sizer = QHBoxLayout()
        ok_button = QPushButton("Ok")
        cancel_button = QPushButton("Cancel")
        button_sizer.addWidget(cancel_button)
        button_sizer.addWidget(ok_button)
        main_sizer.addLayout(button_sizer)
        ok_button.clicked.connect(self.on_ok_button)
        cancel_button.clicked.connect(self.on_cancel_button)

        self.left_list.itemDoubleClicked.connect(self.dir_double_clicked)

        self.show()
    # end initUI

    def on_ok_button(self):
        selected = [ item.text() for item in  self.right_list.selectedItems() ]
        if len(selected) == 0:
            return
        else:
            selected = selected[0]
        self.path = self.sftp.getcwd()
        self.file = selected
        self.success = True
        self.close()
    # end on_ok_button

    def on_cancel_button(self):
        self.path = ''
        self.file = ''
        self.success = False
        self.close()
    # end on_cancel_button

# end class QRemoteFileSelector



